/* tslint:disable */
/* eslint-disable */
/**
*/
export function run(): void;
/**
*/
export function greet(): void;
/**
*
* file: 前端File对象
* title_row: 标题在第几行 组合标题: [2,3]
* rows_excluded: 排除多少行数据, 一行, 二行, 三行: [1,2,3]
* excluded_keyword: 关键字排除: 在单元格中检测到该关键字读取终止
* @param {File} file
* @param {any} title_row
* @param {any} rows_excluded
* @param {any} excluded_keyword
* @returns {any}
*/
export function read_excel_file(file: File, title_row: any, rows_excluded: any, excluded_keyword: any): any;
/**
* @param {File} file
* @param {any} excluded_keyword
* @returns {any}
*/
export function read_excel_file_general(file: File, excluded_keyword: any): any;

export type InitInput = RequestInfo | URL | Response | BufferSource | WebAssembly.Module;

export interface InitOutput {
  readonly memory: WebAssembly.Memory;
  readonly run: () => void;
  readonly greet: () => void;
  readonly read_excel_file: (a: number, b: number, c: number, d: number) => number;
  readonly read_excel_file_general: (a: number, b: number) => number;
  readonly __wbindgen_malloc: (a: number) => number;
  readonly __wbindgen_realloc: (a: number, b: number, c: number) => number;
  readonly __wbindgen_export_2: WebAssembly.Table;
  readonly _dyn_core__ops__function__FnMut__A____Output___R_as_wasm_bindgen__closure__WasmClosure___describe__invoke__h3c06aee94d36deb5: (a: number, b: number, c: number) => void;
  readonly __wbindgen_free: (a: number, b: number) => void;
  readonly __wbindgen_exn_store: (a: number) => void;
  readonly wasm_bindgen__convert__closures__invoke2_mut__he4947550fef6eaab: (a: number, b: number, c: number, d: number) => void;
  readonly __wbindgen_start: () => void;
}

/**
* If `module_or_path` is {RequestInfo} or {URL}, makes a request and
* for everything else, calls `WebAssembly.instantiate` directly.
*
* @param {InitInput | Promise<InitInput>} module_or_path
*
* @returns {Promise<InitOutput>}
*/
export default function init (module_or_path?: InitInput | Promise<InitInput>): Promise<InitOutput>;
